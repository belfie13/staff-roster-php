<?php

$dir = __DIR__.'/../src/';
$classNames = [
    'IndexedList',
    'Process',
    'Role',
    'Roster',
    'Shifts',
    'Staff',
    'Task',
    'Time',
    'Weekday'
 ];

foreach ($classNames as $name)
 {
    require_once $dir.$name.'.php';
 }

$txt = '<?php'.PHP_EOL;
$txt.= 'namespace B13;'.PHP_EOL;
foreach ($classNames as $name)
 {
    $ref = new ReflectionClass('B13\\'.$name);
    if ($ref->isAbstract())
        $txt.= 'abstract ';
    if ($ref->isFinal() && !$ref->isEnum())
        $txt.= 'final ';
    if ($ref->isReadOnly())
        $txt.= 'readonly ';
    if ($ref->isEnum())
        $txt.= 'enum ';
    elseif ($ref->isTrait())
        $txt.= 'trait ';
    elseif ($ref->isInterface())
        $txt.= 'interface ';
    else
        $txt.= 'class ';
    $txt.= $ref->getShortName();
    if ($ref->getParentClass())
        $txt.= ' extends '.$ref->getParentClass()->getName();
    if ($ref->getInterfaceNames())
        $txt.= ' implements '.implode(', ', $ref->getInterfaceNames());
    $txt.= ' {'.PHP_EOL;

    if ($ref->getTraitNames())
        $txt.= '    use '.implode(';'.PHP_EOL.'    use ', $ref->getTraitNames()).';'.PHP_EOL;
    
    $props = $ref->getProperties();
    foreach ($props as $prop)
     {
        $type = ' '.implode(' ', Reflection::getModifierNames($prop->getModifiers())).' '.($prop->getType() ?? '');
        $txt.= '   '.$type.' $'.$prop->getName().';'.PHP_EOL;
     }
    
    $methods = $ref->getMethods();
    foreach ($methods as $meth)
     {
        $mods = implode(' ', Reflection::getModifierNames($meth->getModifiers()));
        $txt.= '    '.$mods.' function '.$meth->getName().'();'.PHP_EOL;
     }

    $txt.= '}'.PHP_EOL;
 }

file_put_contents($dir.'_Summary.phps', $txt);