<?php

/**
 * B13 PHP Library depends on the Infra Standard. {@see https://infra.spec.whatwg.org}
 * 
 * @author CIID Mike Belfie <belfie13@hotmail.com>
 * @copyright 2008-2022 B13 Australia. All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL-3.0-only
 */

declare(strict_types=1);

namespace B13;

use Stringable;


/**
 * 
 */
class IndexedList
 {
    private array $entries = [];
    public function addEntry(string|Stringable|IndexedList $entry)
     {
        $this->entries[] = $entry;
     }
    public function has(int $index): bool
     {
        return isset($this->entries[$index]);
     }
    public function get(int $index): string|Stringable|IndexedList|null
     {
        return $this->entries[$index];
     }
    public function set(int $index, string|Stringable|IndexedList $entry)
     {
        if ($index > $this->count())
         {
            throw new \Exception("Error Processing Request: index out of bounds", 1);
         }
        $this->entries[$index] = $entry;
     }
    public function unset(int $index)
     {
        unset($this->entries[$index]);
     }
    public function toArray(): array
     {
        return $this->entries;
     }

    //                      ~~~>>> ArrayAccess <<<~~~                     //
    /**
     * ArrayAccess
     */
    public function offsetExists(mixed $offset): bool
     {
        return $this->has($offset);
     }
    /**
     * ArrayAccess
     */
    public function offsetGet(mixed $offset): mixed
     {
        return $this->get($offset);
     }
    /**
     * ArrayAccess
     */
    public function offsetSet(mixed $offset, mixed $value): void
     {
        $this->set($offset, $value);
     }
    /**
     * ArrayAccess
     */
    public function offsetUnset(mixed $offset): void
     {
        $this->unset($offset);
     }

    //                       ~~~>>> Countable <<<~~~                      //
    /**
     * Countable
     */
    public function count(): int
     {
        return count($this->entries);
     }

    //                   ~~~>>> IteratorAggregate <<<~~~                  //
    /**
     * IteratorAggregate
     */
    public function getIterator(): Traversable
     {
        return new RecursiveArrayIterator($this->toArray());
     }
 }
