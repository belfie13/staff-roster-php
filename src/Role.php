<?php

/**
 * B13 PHP Library depends on the Infra Standard. {@see https://infra.spec.whatwg.org}
 * 
 * @author CIID Mike Belfie <belfie13@hotmail.com>
 * @copyright 2008-2022 B13 Australia. All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL-3.0-only
 */

declare(strict_types=1);

namespace B13;

/**
 * a staff role defined within the business.
 * eg. operator, salesperson, storeperson, manager.
 */
class Role
 {
    /**
     * Creates from names.
     *
     * @param array $names The name strings.
     * @return array An array of Role objects.
     */
    public static function createFromNames(array $names): array
     {
        $roles = [];
        foreach ($names as $name)
         {
            $roles[] = new Role($name);
         }
        return $roles;
     }
    public function __construct(
        public readonly string $name
    )
     {
     }
 }
