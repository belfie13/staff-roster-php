<?php

/**
 * B13 PHP Library depends on the Infra Standard. {@see https://infra.spec.whatwg.org}
 * 
 * @author CIID Mike Belfie <belfie13@hotmail.com>
 * @copyright 2008-2022 B13 Australia. All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL-3.0-only
 */

declare(strict_types=1);

namespace B13;

class Shifts
 {
    private array $data = [];
    public function addShift(Role $role, Weektime $start, Weektime $end)
     {
        $data[] = ['role' => $role, 'start' => $start, 'end' => $end];
     }
 }
