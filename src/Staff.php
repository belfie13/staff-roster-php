<?php

/**
 * B13 PHP Library depends on the Infra Standard. {@see https://infra.spec.whatwg.org}
 * 
 * @author CIID Mike Belfie <belfie13@hotmail.com>
 * @copyright 2008-2022 B13 Australia. All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL-3.0-only
 */

declare(strict_types=1);

namespace B13;

class Staff
 {
    private string $name;
    private $rolePreferences = [];
    private $roleRatings = [];
    private $totalHoursPreference = 0;
    private $totalHoursAvailable = 0;
    private $workHoursPrefered = []; // time period [start,end]
    private $workHoursAvailable = []; // time period [start,end]
 }
