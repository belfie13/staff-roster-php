<?php

/**
 * B13 PHP Library depends on the Infra Standard. {@see https://infra.spec.whatwg.org}
 * 
 * @author CIID Mike Belfie <belfie13@hotmail.com>
 * @copyright 2008-2022 B13 Australia. All rights reserved.
 * @license https://www.gnu.org/licenses/gpl-3.0.txt GPL-3.0-only
 */

declare(strict_types=1);

namespace B13;

class Time
 {
    /**
     * create from string with format `hours:minutes`.
     */
    public static function create(string $time): Time
     {
        sscanf($time, "%d:%d", $hours, $minutes);
        return new Time($hours, $minutes);
     }
    public function __construct(
        public readonly int $hours,
        public readonly int $minutes
    )
     {}
    
    public function __toString(): string
     {
        return sprintf('%02:%02', $this->hours, $this->minutes);
     }
 }
