<?php
namespace B13;
class IndexedList {
    private array $entries;
    public function addEntry();
    public function has();
    public function get();
    public function set();
    public function unset();
    public function toArray();
    public function offsetExists();
    public function offsetGet();
    public function offsetSet();
    public function offsetUnset();
    public function count();
    public function getIterator();
}
class Process extends B13\IndexedList {
    public function getSteps();
    public function addEntry();
    public function has();
    public function get();
    public function set();
    public function unset();
    public function toArray();
    public function offsetExists();
    public function offsetGet();
    public function offsetSet();
    public function offsetUnset();
    public function count();
    public function getIterator();
}
class Role {
    public readonly string $name;
    public static function createFromNames();
    public function __construct();
}
class Roster {
    private B13\Shifts $shifts;
    private array $data;
    public function __construct();
    public function addStaffShift();
}
class Shifts {
    private array $data;
    public function addShift();
}
class Staff {
    private string $name;
    private  $rolePreferences;
    private  $roleRatings;
    private  $totalHoursPreference;
    private  $totalHoursAvailable;
    private  $workHoursPrefered;
    private  $workHoursAvailable;
}
class Task {
    public function __construct();
    public function getTitle();
    public function getRoles();
    public function getProcess();
}
class Time implements Stringable {
    public readonly int $hours;
    public readonly int $minutes;
    public static function create();
    public function __construct();
    public function __toString();
}
enum Weekday implements UnitEnum {
    public readonly string $name;
    public static function cases();
}
